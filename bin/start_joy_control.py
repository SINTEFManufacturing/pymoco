#!/usr/bin/env python3
# coding=utf-8

"""
Script for starting a simple joystick jogger application for control
of the UR robot over standard TCP port.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2012-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import atexit
import argparse
import pathlib
import logging

import pymoco.robots
import pymoco.facades
from pymoco.ui.joy_manager import JoyManager
from pymoco.controller_manager import ControllerManager

parser = argparse.ArgumentParser()
parser.add_argument('facade_name', type=str,
                    choices=pymoco.facades.get_facade_types())
parser.add_argument('rob_type', type=str,
                    choices=pymoco.robots.get_robot_types())
parser.add_argument('--rob_host', type=str, default='127.0.0.1')
parser.add_argument('--rob_port', type=int, default=5002)
parser.add_argument('--rob_in_port', type=int, default=20021)
parser.add_argument('--rob_out_port', type=int, default=20020)
parser.add_argument('--joy_path', type=pathlib.Path,
                    default=pathlib.Path('/dev/input/js0'))
args = parser.parse_args()

cm = ControllerManager(
    facade_name=args.facade_name,
    rob_type=args.rob_type,
    rob_host=args.rob_host,
    rob_port=args.rob_port,
    rob_in_port=args.rob_in_port,
    rob_out_port=args.rob_out_port,
    log_level=logging.INFO)

atexit.register(cm.stop)

jm = JoyManager([cm], start_index=0, js_dev_name=args.joy_path)
