#!/usr/bin/python
# coding=utf-8

"""
Joint jogger implementation module.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2011-2013"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"


import sys
import os
from PyQt4 import QtCore, QtGui
import numpy

from joint_jogger_ui import Ui_JointJogger
from joint_jogger_control import JointJoggerControl

#pymoco.setup()
#pymoco.startjlc()

class JointJogger(QtGui.QMainWindow, Ui_JointJogger):

    def __init__(self, argv): #, llcp, jlc):
        #self._llcp = llcp
        #self._jlc = jlc
        QtGui.QMainWindow.__init__(self)
        self._jjc = JointJoggerControl()
        self._jjc.start()
        self.setupUi(self)
        self.robselectCB.addItems(['<disabled>', 'ur', 'sc15f', 'sh133'])
        self._jsliders =  [eval('self.jointSlider_%d'%i) for i in xrange(6)]
        self._jlcdispl = [eval('self.jointValueDSB_%d'%i) for i in xrange(6)]
        self._factor = 1000.0
        self._ifactor = 1.0/self._factor
        QtCore.QObject.connect(self.robselectCB,  QtCore.SIGNAL('activated (QString)'), self.set_robot)
        QtCore.QObject.connect(self.controlTW, QtCore.SIGNAL('currentChanged(int)'), self._changed_control_tab)
        QtCore.QObject.connect(app, QtCore.SIGNAL('aboutToQuit()'), self.shutdown)
        QtCore.QObject.connect(self.aMax, QtCore.SIGNAL('valueChanged(double)'), self._set_max_acceleration)
        QtCore.QObject.connect(self.targetSpeed, QtCore.SIGNAL('valueChanged(double)'), self._set_target_speed)
        self._set_target_speed(self.targetSpeed.value())
        self._set_max_acceleration(self.aMax.value())
        self.setWindowIcon(QtGui.QIcon('empty.ico.png'))

    def set_robot(self, robotid):
        print('Setting robot : %s' % str(robotid))
        self._jjc.set_robot(robotid, llcInPort=self.llc_in_port.value(), llcOutPort=self.llc_out_port.value())
        if robotid == '<disabled>':
            self.setWindowIcon(QtGui.QIcon('empty.ico.png'))
            for i in xrange(6):
                js = self._jsliders[i]
                js.setEnabled(False)
        else:
            robiconfile = robotid + '.ico.png'
            if os.path.isfile(robiconfile):
                robicon = QtGui.QIcon(robiconfile)
            else:
                robicon = QtGui.QIcon('empty.ico.png')
            self.setWindowIcon(robicon)
            self._posLims = self._jjc.get_pos_limits()
            self._changed_control_tab(self.controlTW.currentIndex())

    def _set_max_acceleration(self, aMax):
        self._jjc._acc_max = aMax

    def _set_target_speed(self, targetSpeed):
        self._jjc._target_speed = targetSpeed
    
    
    def _enable_position_control(self):
        qact = self._jjc.get_pos()
        self.posTab.setEnabled(True)
        for i in xrange(6):
            js = self._jsliders[i]
            js.setEnabled(True)
            jlcd = self._jlcdispl[i]
            js.setMinimum(int(self._factor * self._posLims[0][i]))
            js.setMaximum((self._factor * self._posLims[1][i]))
            js.setValue(int(self._factor * qact[i]))
            jlcd.setValue(qact[i])
            QtCore.QObject.connect(js, QtCore.SIGNAL('valueChanged(int)'), self.joint_control_change)
            QtCore.QObject.connect(js, QtCore.SIGNAL('sliderReleased()'), self.slider_release)

    def _changed_control_tab(self, i):
        if i == 0:
            self._jjc.position_control()
            self._enable_position_control()
        elif i == 1:
            self._jjc.velocity_control()

    def slider_release(self):
        # qact = self._jjc.get_pos()
        # print('Sliders released, setting actual position as target.')
        # self._jjc.set_pos_target(qact)
        print('Sliders released, commanding robot to rest.')
        self._jjc.go_to_rest()
        print('Waiting for rest')
        self._jjc.wait_for_rest()
        print('Robot is resting, sync\'ing sliders.')
        self._sync_sliders()

    def _sync_sliders(self):
        qact = self._jjc.get_pos()
        for i in xrange(6):
            self._jlcdispl[i].setValue(qact[i])
            self._jsliders[i].setValue(int(self._factor * qact[i]))

    def joint_control_change(self):
        qact = self._ifactor * numpy.array([js.value() for js in self._jsliders])
        self._jjc.set_pos_target(qact)
        for i in xrange(6): self._jlcdispl[i].setValue(qact[i])

    def shutdown(self):
        print('Shutting down')
        #self._jjc.set_robot('<disabled>')
        self._jjc.stop()
        
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    jj = JointJogger(sys.argv) #pymoco.llcp, pymoco.jlc)
    jj.show()
    sys.exit(app.exec_())

