# coding=utf-8
"""
Module for management class for joystick control of robots. 
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2012-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

from .joy_controller import JoyController
from .joy_device import JoyDevice

class JoyManager(object):
    def __init__(self, con_mans, js_dev_name=None, start_index=-1):
        self._jdev = JoyDevice(js_dev_name=js_dev_name)
        self._jdev.start()
        self._jc = JoyController(jdev=self._jdev)
        self._con_mans = con_mans
        self._index = -1
        if start_index >= 0:
            self(start_index)
        
    def __call__(self, index=0):
        if index == self._index:
            return self._jc
        if self._jc is not None and self._jc.is_alive():
            print('Current Joy Controller must be stopped before switching!')
        else:
            self._index = index
            self._jc = JoyController(
                self._con_mans[self._index].tvc(), jdev=self._jdev)
            self._jc.start()

    def stop_joy(self):
        if self._index >= 0:
            print('Stopping Joy Controller. An event must be generated, '
                  + 'so press any button.')
            self._jc.stop()
            self._jc.join()
            self._index = -1


