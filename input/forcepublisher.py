# coding=utf-8
"""
Module implementing a force publisher for a 6D force sensor.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2011"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import threading
import socket
import time
import struct
import traceback

import numpy


class ForcePublisher(threading.Thread):
    """A listener to the force sensor server. Publishes forces to
    subscribers."""
    def __init__(self, **kwargs):
        self._subscribers = []
        self._stop_flag = threading.Event()
        threading.Thread.__init__(self)
        threading.Thread.setDaemon(self,1)
        self._forceHost = kwargs.get('forceHost','')
        self._forcePort = kwargs.get('forcePort', 5559)
        self._forceConn = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._forceConn.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._forceConn.bind((self._forceHost, self._forcePort))
        self._forceConn.settimeout(1)
        self._wrenchLock = threading.Lock()
        self._loglev = 0
        self._wrench = numpy.zeros(6)

    def _log(self, msg, level=1):
        if level <= self._loglev:
            print(self.__class__.__name__ + '::' +
                  traceback.extract_stack(limit=2)[-2][2] + ' : ' + msg)

    def subscribe(self,sub):
        if not sub in self._subscribers:
            self._subscribers.append(sub)

    def unsubscribe(self,sub):
        if sub in self._subscribers:
            self._subscribers.remove(sub)
        
    def run(self):
        while not self._stop_flag.isSet():
            data = ''
            try:
                data,addr = self._forceConn.recvfrom(1024)
            except:
                self._log('Timeout on socket',2)
            else:
                wrench = numpy.array(struct.unpack('dddddd',data))
                self._log('%+03.3f '*6%tuple(wrench),3)
                with self._wrenchLock:
                    self._wrench = wrench.copy()
                ## Notify subscribers
                for s in self._subscribers:
                    try:
                        s.forceNotify(wrench)
                    except:
                        self._log('Exception when trying to notify '
                                  + 'subscriber %s. REMOVING!!'%str(s),2)
                        self.unsubscribe(s)
                    
        self._forceConn.close()
        
    def getForce(self):
        with self._wrenchLock:
            w = self._wrench.copy()
        return w

    def stop(self):
        self._stop_flag.set()
        

class DummyForceSubscriber(object):
    def notifyForce(self,wrench):
        print('%3.4f ' % time.time(), ':', ' %+3.3f' * 6 % tuple(wrench))
        
