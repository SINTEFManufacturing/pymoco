# coding=utf-8
"""
Module for listening for joint configuration packets from the
Universal Robots UR5 LLC accessor, and publishing the information
internally to subscribers.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2011-2021"
__credits__ = ["Morten Lind", "Johannes Schrimpf"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import time
import traceback
import struct
import threading
import socket
import logging

import numpy as np

from . import llcpublisher


class URGwPublisher(llcpublisher.LLCPublisher):
    """A class for publishing information from packets sent by the
    P-interface to the LLC-'Router' implemented on the UR controller
    platform."""

    class Error(Exception):
        ''' Error class for thowing exceptions.'''

        def __init__(self, message):
            Exception.__init__(self)
            self.message = message

        def __repr__(self):
            return self.__class__.__name__ + ' : ' + self.message

    # Important communication constants
    bufsz = 1024
    # The "Router" sends in every cycle what is read off the servo
    # interface. This comprises six doubles for joint positions in
    # radians appended by six doubles for joint velocities in
    # rads/s. In addition, a protocol ID in one unsigned char and a
    # cycle frame number in unsigned long are prepended.
    p_struct = struct.Struct('<BL6d6d')

    def __init__(self, **kwargs):
        """Binding to 'host' try to read off controller packets from
        'gw_out_port'."""
        llcpublisher.LLCPublisher.__init__(self, **kwargs)
        # Simplify the actuator position vector as a view on serial
        self._p_actuator = self._p_serial.view()
        self._protocol_id = None
        self._cycle_number = None
        self._gw_socket = kwargs.get('gw_socket', None)
        if self._gw_socket is None:
            # A socket was not handled externally, create one.
            # Get the out port from the llc. Use default from the input module.
            self._bind_host = kwargs.get('bind_host', '')
            self._gw_host = kwargs.get('gw_host')
            self._gw_out_port = kwargs.get('gw_out_port')
            self._gw_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self._gw_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self._gw_socket.bind((self._bind_host, self._gw_out_port))
            # self._gw_socket.connect((self._gw_host, self._gw_out_port))
            self._gw_socket.settimeout(1.0)
            # The host of llc that packets are received from
            # self._gw_host = kwargs.get('llc_host', None)
        else:
            self._log.info('Getting host and port from existing socket.')
            self._gw_host, self._gw_out_port = self._gw_socket.getsockname()
        self._log = logging.getLogger(
            f'{self.__class__.__name__}<{self._gw_host}:{self._gw_out_port}')
        self._log.setLevel(kwargs.get('log_level', logging.INFO))
        self._log.info('Bound to "{:s}:{:d}"'
                       .format(self._gw_host, self._gw_out_port))
        # Default initial definition of configuration variables
        self._packet_recv_fails = 0
        self._last_p_time = time.time()
        threading.Thread.__init__(self,
                                  name='URGwPublisher <- {:s}:{:d}'
                                  .format(self._gw_host, self._gw_out_port))
        self.daemon = True

    def get_llc_host(self):
        return self._gw_host
    llc_host = property(get_llc_host)

    def _initialize(self):
        ''' Initialisation method for trying to establish a connection
        to the accessor.'''
        self._log.info('Starting up on llc host %s, port %d' %
                       (self._gw_host, self._gw_out_port))
        self._recv_p_packet()
        self._parse_p_packet()
        # Ensure that the frame computer is initialized.
        self._rob_fac.frame_computer(self._p_serial)
        self._log.info('LLC Packet received, setting initialized flag.')
        self._initialized_event.set()
        self._log.info('Notifying the initial subscribers.')
        self._notify_subscribers(self._p_time)
        self._log.info('Initialised OK. Positions: ' + str(self._p_serial))
        return

    def _parse_p_packet(self):
        """ Parse the current llc to main packet, reading the llc
        position."""
        if self._p_data is not None:
            # Received encoder values
            data_vec = self.p_struct.unpack(self._p_data)
            self._protocol_id = data_vec[0]
            self._rob_fac.cycle_number = data_vec[1]
            actual_pos = np.array(data_vec[2:8])
            actual_vel = np.array(data_vec[8:14])
            with self._p_lock:
                self._p_encoder_vel[:] = actual_vel
                self._p_encoder[:] = actual_pos
                self._p_serial[:] = self._rob_def.encoder2serial(self._p_encoder)
                self._p_serial_vel[:] = (self._rob_def._encoder_gains *
                                         self._p_encoder_vel)
            self._log.debug('Received packet of protocol "%d" for cycle number "%d".'
                            % tuple(data_vec[:2]))

    def _recv_packet(self):
        ''' Private method for trying to receive a packet from the
        accessor.'''
        try:
            packet = self._gw_socket.recv(self.bufsz)
            ptime = time.time()
            self._log.debug('Received packet of length %d' % len(packet))
        except socket.timeout:
            # Socket timout
            self._log.debug('LLC socket timeout')
            self._packet_recv_fails += 1
            return -1.0, ''
        except socket.error as emsg:
            # Socket error
            self._log.warn('LLC socket general error : %s' % str(emsg))
            self._packet_recv_fails += 1
            return -1.0, ''
        else:
            # # Received a packet
            self._packet_recv_fails = 0
            return ptime, packet

    def _recv_p_packet(self, tries=3):
        ''' Receive an LLC to main controller packet.'''
        # First test that we have a connection by receiving one packet.
        ptime, packet = self._recv_packet()
        if len(packet) == 0:
            # Assume no connection
            self._log.warn('Received empty packet. Disconnected?')
            return False
        # Assume we can get packets
        while tries > 0:
            self._log.debug('Received packet of size %d' % len(packet))
            if len(packet) == self.p_struct.size:
                break
            else:
                self._log.warn('Wrong packet size %d. Expected %d' %
                               (len(packet), self.p_struct.size))
                ptime, packet = self._recv_packet()
            tries -= 1
        if tries == 0:
            return False
        with self._p_lock:
            self._p_data = packet
            self._p_time = ptime
        return True

    def run(self):
        ''' Main loop of the llc publisher: Get and process
        packets, notifying subscribers.'''
        self._log.info('Starting up on llc host %s, port %d' %
                       (self._gw_host, self._gw_out_port))
        self._initialize()
        while not self._stop_flag.isSet() and self._packet_recv_fails < 10:
            if self._recv_p_packet():
                self._parse_p_packet()
                self._notify_subscribers(self._p_time)
                self._period_est = (0.1 * (self._p_time - self._last_p_time)
                                    + 0.9 * self._period_est)
                self._last_p_time = self._p_time
            else:
                self._log.error('Receive of packet failed. Disconnected socket '
                                + 'inferred. Raising exception')
                raise URGwPublisher.Error('Disconnected socket detected.')
        self._gw_socket.close()
        self._log.info('Stopped.')
