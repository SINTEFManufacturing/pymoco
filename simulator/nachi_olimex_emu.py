# coding=utf-8
"""
Module for basic emulation of the Olimex based real-time interaction
with the Nachi AX controllers.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2012-2014"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

import struct
import socket
import time
import threading
import types
import traceback

import numpy as np
import math3d as m3d

import pymoco.kinematics
import pymoco.control
import pymoco.input

from .robot_emu import find_link_chain

class NachiOlimexEmu(threading.Thread):
    """ Emulation of the Nachi Olimex controller for the blender robot model."""

    ## Structs for receiving position commands
    in_struct_ang = struct.Struct('<6d')
    in_struct_enc = struct.Struct('<' + 12 * 'BBi')
    ## Structs for sending main to servo and olimex to servo commands
    mtos_out_struct_enc = struct.Struct('<6L6L')
    mtos_out_struct_ang = struct.Struct('<6d6d')
    ## Structs for sending servo (to main) status
    stom_out_struct_enc = struct.Struct('<6L')
    stom_out_struct_ang = struct.Struct('<6d')

    # Struct for PPM-proto
    io_struct_enc_ppm = struct.Struct('<12i')

    def __init__(self, rob_base_go, rob_def, bind_host='127.0.0.1', proto='PPM',
                 olimex_in_port=pymoco.control.llc_in_port,
                 olimex_out_port=pymoco.input.llc_out_port,
                 log_level=2):
        self._stop_flag = False
        ## Socket port for receiving commanded joint positions
        self._bind_host = bind_host
        self._olimex_in_port = olimex_in_port
        ## Socket port for emitting actual joint positions
        self._olimex_out_port = olimex_out_port
        self._log_level = log_level
        ## The game object for the robot base 
        self._base_go = rob_base_go
        self._rob_def = rob_def
        self._proto = proto
        self._metres_per_unit = self._base_go.get('metres_per_unit', 1.0)
        self._units_per_metre = 1.0 / self._metres_per_unit
        threading.Thread.__init__(self)
        self._dataLock = threading.Lock()
        self._q = self._rob_def.q_home.copy()
        self._actuator = self._rob_def.serial2actuator(self._q)
        self._actuator_zero = np.zeros(6, dtype=np.double)
        self._encoder_zero = self._rob_def.actuator2encoder(
            self._actuator_zero).astype(np.long)
        ## Base transform and frame computer
        self._base_xform=m3d.Transform(
          m3d.Orientation(np.array(self._base_go.worldOrientation)),
          m3d.Vector(self._metres_per_unit
                     * np.array(self._base_go.worldPosition)))
        self._log('Found robot base at position %s' %
              str(self._base_xform._v),2)
        self._fc = pymoco.kinematics.FrameComputer(
            q=np.zeros(6), base_xform=self._base_xform,
            rob_def=self._rob_def, cache_in_frames=True)
        self._log('Kinematics report tool position at %s' %
              str(self._fc()._v))
        ## Make an ordered list of links
        self._links = find_link_chain(self._base_go)
        self._log('Robot links: %s' % str(self._links),2)
        self._set_q(self._q)
        self._olimex_in_socket=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._olimex_in_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._log('Binding to "%s" on port %s'%(self._bind_host,
                                                self._olimex_in_port),2)
        self._olimex_in_socket.bind((self._bind_host, self._olimex_in_port))
        self._olimex_in_socket.settimeout(0.005)
        self._olimex_out_socket = socket.socket(socket.AF_INET, 
                                                socket.SOCK_DGRAM)
        self._olimex_out_socket.setsockopt(socket.SOL_SOCKET, 
                                           socket.SO_BROADCAST, 1)
        if self._bind_host in ['localhost', '127.0.0.1']:
            self.syncaddr = ('127.0.0.1', self._olimex_out_port)
            #self.syncaddr=(self._bind_host,self._olimex_out_port)
        else:
            self.syncaddr = ('255.255.255.255', self._olimex_out_port)
        self._log('Syncing to address ' + str(self.syncaddr))
        self._t_cur = time.time()
        self._delta_t = 0.05
        self._t_old = self._t_cur - self._delta_t
        self._tLastLog = self._t_cur
        
    def _log(self, msg, log_level=2):
        if log_level <= self._log_level:
            print(str(log_level) + ' :  NachiOlimexEmu (%d,%d)::'
                  % (self._olimex_in_port, self._olimex_out_port)
                  + traceback.extract_stack(limit=2)[-2][2] +' : '
                  + msg)

    def parse_actuator_packet(self, packet):
        if self._proto == 'PPM':
            if len(packet) == self.io_struct_enc_ppm.size:
                enc_plus = np.array(self.io_struct_enc_ppm.unpack(packet))[:6]
            else:
                self._log('!! Wrong packet size. !! Expected {}, received {}'
                      .format(self.in_struct_ang.size,len(packet)), 1)
                enc_plus = None
        elif self._proto == 'SRM':
            if len(packet) == self.in_struct_enc.size:
                enc_deltas = np.array(self.in_struct_enc.unpack(packet))
                enc_deltas = enc_deltas[2::3]
                ## We use only the first encoder set, the plus-encoder,
                ## since main is kept at zero
                enc_plus = enc_deltas[:6]
            else: 
                self._log('!! Wrong packet size. !!  Expected {}, received {}' 
                          .format(self.in_struct_enc.size,len(packet)), 1)
                enc_plus = None
        else:
            raise Error('Unknown protocol {}'.format(self._proto))
        actuator = self._rob_def.encoder2actuator(enc_plus +
                                                  self._encoder_zero)
        return actuator

    def try_recv_packet(self):
        try:
            packet = self._olimex_in_socket.recv(1024)
        except socket.error:
            pass
        else:
            ## Receive a timestamp and a joint actuator configuration
            actuator = self.parse_actuator_packet(packet)
            if not actuator is None:
                q = self._rob_def.actuator2serial(actuator)
                self._set_q(q)
                #self._dataLock.acquire()
                with self._dataLock:
                    self._q = q
                    self._actuator = actuator
                    if self._log_level >= 5: self._log('Received actuator position :'+str(self._actuator), 5)

    def run(self):
        self._initialising = True
        self._stop_flag = False
        emits = 0
        while self._initialising and emits < 10 and not self._stop_flag:
            self.emit_state()
            time.sleep(0.1)
            emits += 1
        self._initialising = False
        while not self._stop_flag:
            self.try_recv_packet()
        self._olimex_out_socket.close()
        self._olimex_in_socket.close()
        self._log('Stopped.',1)

    def emit_state(self):
        if self._stop_flag:
            return
        ## Pack the stom package
        with self._dataLock:
            actuator = self._actuator.copy()
            q = self._q.copy()
        servo_enc = self._rob_def.actuator2encoder(actuator).astype(np.long)
        ## The stom package has the same structure as the input
        ## package, so we cheat and use it.
        if self._proto == 'PPM':
            self._ms_packet = self.io_struct_enc_ppm.pack(*(np.append(self._encoder_zero, servo_enc).tolist()))
            if self._log_level >= 5:
                self._log('Sending main and servo encoder package of length {}'
                          .format(len(self._ms_packet),5))
            ## Send the data
            self._olimex_out_socket.sendto(self._ms_packet, self.syncaddr)
        elif self._proto == 'SRM':
            self._stom_packet = self.stom_out_struct_enc.pack(*servo_enc.tolist())
            self._mtos_packet = self.mtos_out_struct_enc.pack(
                *np.append(self._encoder_zero, servo_enc).tolist())
            ## Send the data
            self._olimex_out_socket.sendto(self._stom_packet, self.syncaddr)
            self._olimex_out_socket.sendto(self._mtos_packet, self.syncaddr)
        self._t_cur = time.time()
        self._delta_t = 0.5 * self._delta_t + 0.5 * (self._t_cur - self._t_old)
        if self._t_cur - self._tLastLog > 2:
            self._log('Frame-rate est. : %.2f' % (1.0 / self._delta_t),5)
            self._tLastLog = time.time()
        self._t_old = self._t_cur
        time.sleep(0.001)
        
    def _set_q(self, q):
        self._fc.joint_angles_vec = q
        frames = self._fc.get_in_frame(array=True)
        for l, f in zip(self._links,frames):
            l.worldOrientation = f[:3,:3].tolist()
            l.worldPosition = (self._units_per_metre * f[:3,3]).tolist()
            
    def stop(self):
        self._stop_flag = True
