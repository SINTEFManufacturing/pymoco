#!/usr/bin/env python3
import sys
import numpy as np
resp_time_file = 'rts.npy'
if len(sys.argv) > 1:
    resp_time_file = sys.argv[1]
rts=np.load(resp_time_file);print(np.max(rts), np.average(rts[np.where(rts>0.0)]))
