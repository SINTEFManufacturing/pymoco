# coding=utf-8
"""
Module for kinematics computation facilities.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2011-2022"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

import time

import math3d as m3d
import numpy as np


class FrameComputer(object):
    """ Given a joint configuration vector, computes and holds all
    frames in the transformation chain from base to tool flange."""

    class Error(Exception):
        def __init__(self, message):
            self.message = message

        def __repr__(self):
            return self.__class__.__name__ + '.Error :' + self.message

    _tool_xform = np.identity(4)

    @classmethod
    def set_common_tool_xform(cls, tool_transform):
        if type(tool_transform) == m3d.Transform:
            cls._tool_xform = tool_transform.array
        elif (type(tool_transform) == np.ndarray
              and tool_transform.shape == (4, 4)):
            cls._tool_xform = tool_transform.copy()
        else:
            raise FrameComputer.Error(
                'Must have Transform or array for ' +
                'creating Transform as argument.')
    setCommonToolXForm = set_common_tool_xform

    def __init__(self, q=None, base_xform=m3d.Transform(),
                 tool_xform=m3d.Transform(), rob_def=None,
                 cache_out_frames=True, cache_in_frames=False):
        """Initialise a FrameComputer with given base and tool
        transforms (defaulting to identities) and given robot
        definition. Flags for caching inward and outward link frames
        may be given. Outward frames must be cached for computing the
        Jacobian."""
        self._rob_def = rob_def
        if self._rob_def is None:
            raise FrameComputer.Error(
                'Must have a robot definition in argument "rob_def"!')
        self._rob_def._frame_comp = self
        self.refresh_link_xforms()
        self._cache_out_frames = cache_out_frames
        if self._cache_out_frames:
            self._out_frames = np.array(
                [np.identity(4)
                 for t in range(self._rob_def.dof + 1)])
        self._cache_in_frames = cache_in_frames
        if self._cache_in_frames:
            self._in_frames = np.array(
                [np.identity(4)
                 for t in range(self._rob_def.dof + 1)])
        self._q = None
        self._valid_dof = 0
        self._dof = self._rob_def.dof
        self._flange_pose_array = np.identity(4)
        self._tool_pose_array = np.identity(4)
        self._qtime = None
        self._jac = np.ones((6, self._dof), dtype=np.float64)
        self._jtime = None
        self._ijtime = None
        self._joint_xforms = self._rob_def.get_joint_xforms()
        self.base_xform = base_xform
        self.tool_xform = tool_xform
        if q is not None:
            self.joint_angles_vec = q

    def __del__(self):
        if id(self) == id(self._rob_def._frame_comp):
            self._rob_def._frame_comp = None

    def refresh_link_xforms(self):
        """Get a fresh copy of the link transforms from the robot
        definition. Kinematic parameters may have changed, and thus
        the link transforms must be refreshed in the frame computer."""
        self._link_xforms = [lt.array for lt in self._rob_def.link_xforms]

    def get_tool_xform(self, array=False):
        """Get the tool transform used for the kinematic chain. This
        is the static transform from operation (tool) frame to tool
        flange frame."""
        if array:
            return self._tool_xform
        else:
            return m3d.Transform(self._tool_xform)

    def set_tool_xform(self, new_tool_xform):
        """Set the tool transform used for the kinematic chain. This
        is potentially dangerous, since there is no controlled
        transition from the current to the new tool transform;
        i.e. any operating controller may experience a discontinous
        shift in position of the operating pose. The provided
        'new_tool_xform' may be provided as either a m3d.Transform or
        an (4,4) np.ndarray."""
        if type(new_tool_xform) == m3d.Transform:
            self._tool_xform = new_tool_xform.array
        elif (type(new_tool_xform) == np.ndarray
              and new_tool_xform.shape == (4, 4)):
            self._tool_xform = new_tool_xform.copy()
        else:
            raise FrameComputer.Error(
                'Must have Transform or array for setting tool transform.')
        # Trigger a new calculation to update the tracked tool pose
        if self._q is not None:
            self.set_joint_angles_vec(self._q, force_comp=True)
        
    def clear_tool_xform(self):
        """Clear the current tool transform. This falls back to using
        the class common tool xform. To set the identity, set this
        directly."""
        if self.has_special_tool_xform:
            del self._tool_xform
        # Trigger a new calculation to update the tracked tool pose
        if self._q is not None:
            self.set_joint_angles_vec(self._q, force_comp=True)

    # Property for tool transform
    tool_xform = property(get_tool_xform, set_tool_xform, clear_tool_xform)

    def has_special_tool_xform(self):
        return id(self._tool_xform) != id(self.__class__._tool_xform)

    def get_base_xform(self, array=False):
        """Return the base transform, transforming robot base flange
        coordinates to externally defined coordinates."""
        if array:
            return self._base_xform.copy()
        else:
            return m3d.Transform(self._base_xform)

    def set_base_xform(self, new_base_xform):
        if type(new_base_xform) == m3d.Transform:
            self._base_xform = new_base_xform.array
        elif (type(new_base_xform) == np.ndarray
              and new_base_xform.shape == (4, 4)):
            self._base_xform = new_base_xform.copy()
        else:
            raise FrameComputer.Error(
                'Must have Transform or '
                + 'array for setting base transform.')
        # Trigger a new calculation to update the tracked tool pose
        if self._q is not None:
            self.set_joint_angles_vec(self._q, force_comp=True)

    # Property for base transform
    base_xform = property(get_base_xform, set_base_xform)

    def __repr__(self):
        if '_tool_frame' in self.__dict__:
            return 'FrameComputer, Tool Frame:\n'+str(self._tool_pose_array)

    def get_joint_angles_vec(self):
        """Get the current joint angles vector."""
        return self._q.copy()

    def set_joint_angles_vec(self, q, eps=1e-9, force_comp=False):
        """Set the current joint angles vector. All frames are computed
        according to the joint configuration given in 'q'. However, if
        'q' compares equal to the cached joint position vector withing
        'eps', no computation will be performed, unless 'force_comp'
        evaluates to True. The stored joint angles 'self._q' will
        always take a copy of the given joint angle array 'q'.
        """
        # if len(q) != len(self._q) or (q != self._q).any():
        # if not type(q) == np.ndarray:
        q = np.array(q)
        if (self._q is not None
                and not force_comp
                and np.linalg.norm(q-self._q) < eps):
            # No need to recompute
            return
        self._q = q
        # Adjust for home config
        self._valid_dof = len(q)
        self._qtime = time.time()
        t = self._base_xform
        for i, qi in enumerate(q):
            if self._cache_in_frames:
                self._in_frames[i] = t
            lti = self._link_xforms[i]
            if lti is not None:
                t = t.dot(lti)
            if self._cache_out_frames:
                self._out_frames[i] = t
            t = t.dot(self._joint_xforms[i](qi))
        if self._cache_in_frames:
            self._in_frames[self._dof] = t
        lt_end = self._link_xforms[self._dof]
        if lt_end is not None:
            self._flange_pose_array = t.dot(lt_end)
        else:
            self._flange_pose_array = t
        if self._cache_out_frames:
            self._out_frames[self._dof] = self._flange_pose_array
        self._tool_pose_array = self._flange_pose_array.dot(self._tool_xform)
    
    joint_angles_vec = property(get_joint_angles_vec, set_joint_angles_vec)

    # def get_joint_encoder_vec(self):
    #     return  self._rob_def.serial2encoder(self.joint_angles_vec)
    # def set_joint_encoder_vec(self, q_enc):
    #     self.joint_angles_vec = self._rob_def.encoder2serial(q_enc)

    def jacobian(self, q=None):
        """Compute the jacobian of the current joint configuration. It
        is assumed that all frames have been computed. The jacobian
        will be a 6xDoF matrix where the upper 3xDoF matrix gives
        displacement rates of the infinitesimal joint displacements,
        and the lower 3xDoF matrix gives rotation rates in base
        coordinates. When called the time stamp of the Jacobian is
        first compared to the time stamp of the joint configuration,
        and no computation is done if the Jacobian is newer."""
        if not self._cache_out_frames:
            raise FrameComputer.Error(
                'Out-frames are not cached! ' +
                '(Necessary for computing the Jacobian)')
        if q is not None:
            self.joint_angles_vec = q
        if self._jtime is None or self._jtime < self._qtime:
            self._jtime = time.time()
            # Tool frame point (tool center point)
            tfp = self._tool_pose_array[:3, 3]
            for i in range(self._dof):
                # Use only the DoF innermost out frames for defining
                # the joint axes
                ofr = self._out_frames[i]
                jpos = ofr[:3, 3]
                jax = ofr[:3, 2]
                self._jac[:3, i] = np.cross(jax, tfp-jpos)
                self._jac[3:, i] = jax
        return self._jac

    def inverse_jacobian(self, q=None, rcond=10e-7):
        if q is not None:
            self.joint_angles_vec = q
        if self._ijtime is None or self._ijtime < self._qtime:
            self._ijtime = time.time()
            self._inv_jac = np.linalg.pinv(self.jacobian(), rcond=rcond)
        return self._inv_jac

    def get_out_frame(self, n=None, q=None, array=False):
        """Return the n'th outward link frame, or all if n is None. If 'q' is
        given, it is first set as the joint positions. If 'array' is
        flagged, out-frames are returned as a list of arrays, otherwise
        a list of m3d.Transform objects."""
        if not self._cache_out_frames:
            raise FrameComputer.Error('Out-frames are not cached!')
        if q is not None:
            self.joint_angles_vec = q
        if n is None:
            return [m3d.Transform(self._out_frames[i])
                    for i in range(self._valid_dof)]
        elif n <= self._valid_dof:
            return m3d.Transform(self._out_frames[n])
        else:
            raise FrameComputer.Error(
                'Argument "n" must be less than valid DoF, or None.')

    out_frames = property(get_out_frame)
    out_frames_array = property(lambda self: self.get_out_frame(array=True))

    def get_in_frame(self, n=None, q=None, array=False):
        """Return the n'th inward link frame, or all if n is None. If 'q' is
        given, it is first set as the joint positions. If 'array' is
        flagged, in-frames are returned as a list of arrays, otherwise
        a list of m3d.Transform objects.
        """
        if not self._cache_in_frames:
            raise FrameComputer.Error('In-frames are not cached!')
        if q is not None:
            self.joint_angles_vec = q
        if n is None:
            if array:
                return self._in_frames
            else:
                return [m3d.Transform(self._in_frames[i])
                        for i in range(self._valid_dof)]
        elif n <= self._valid_dof:
            if array:
                return self._in_frames[n]
            else:
                return m3d.Transform(self._in_frames[n])
        else:
            raise FrameComputer.Error(
                'Argument "n" must be less than valid DoF, or None.')

    in_frames = property(get_in_frame)
    in_frames_array = property(lambda self: self.get_in_frame(array=True))


    def get_tool_pose(self):
        """Get the tool frame pose in base reference as a m3d.Transform object
        """
        return m3d.Transform(self._tool_pose_array)

    tool_pose = property(get_tool_pose)

    def __call__(self, q=None):
        if q is not None:
            self.joint_angles_vec = q
        return self.tool_pose

    def get_tool_pose_array(self):
        """Get the current tool frame in base reference. The tool frame is
        represented as a 4x4 array
        """
        return self._tool_pose_array.copy()

    tool_pose_array = property(get_tool_pose_array)

    def get_flange_pose(self):
        """Get the current tool flange frame in base reference as a
        m3d.Transform object.
        """
        return m3d.Transform(self._flange_pose_array)

    flange_pose = property(get_flange_pose)

    def get_flange_pose_array(self):
        """Get the current tool flange frame in base reference as a 4x4 array.
        """
        return self._flange_pose_array.copy()

    flange_pose_array = property(get_flange_pose_array)


def infrot(oB0, oB1):
    """Compute a (drx,dry,drz) vector representing the small
    rotational differential between o0 to o1. This method uses the
    approximation that a small-rotation matrix can be approximated by:

    [  1    -drz   dry ]
    [  drz    1   -drx ]
    [ -dry   drx    1  ]
    """
    # Compute the rotation between orientation o0 and o1, i.e. the
    # orientation of o1 as seen in o0.
    o01 = oB0.inverse * oB1
    # Compute the infinitesimal rotation vector between frame o0 and
    # o1, in o0-reference
    ir001 = np.array([o01[2, 1], o01[0, 2], o01[1, 0]])
    # Transform the rotation, to base reference
    irB01 = oB0._data.dot(ir001)
    return irB01


def trfdisp(trf0, trf1):
    """Return the displacement vector, (dpx,dpy,dpz,drx,dry,drz) from
    trf0 to trf1. The positional components are trivial and the
    rotational components are computed by 'infrot'.
    """
    return np.array(((trf1._v-trf0._v)._data,
                     infrot(trf0._o, trf1._o))).flatten()


def testFF(f, q):
    t0 = time.time()
    for i in range(1000):
        f(q)
    cps = (time.time()-t0)*0.001
    print(cps)
    return cps
