# coding=utf-8
"""
Module for differential inverse kinematics solver.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2022"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import logging
import copy

import numpy as np
import math3d as m3d

from .frame_computer import FrameComputer


class IDKSolver:

    """A solver for inverse differential kinematics with joint limit
    prevention.
    """

    def __init__(
            self,
            # Configured frame computer for the specific robot
            frame_computer: FrameComputer,
            # Joints to exclude from a solution; mostly relevant
            # with redundant robots
            excl_joints: list[int] = [],
            # Maximum normed step [rad] to centre joints in their
            # range within the Jacobian's null-space; disabled if <
            # 0.0
            max_jla_step: float = 0.01,
            # Maximum step value for each iteration in the
            # uniform norm in E+(3)
            max_du_step: float = 0.1,
            # Maximum number of iterations to allow
            max_iter: int = 5,
            # Positional tolerance for a solution [m]
            pos_tol: float = 1e-3,
            # Angular tolerance for a solution [rad]
            ang_tol: float = 1e-3):
        self._fc = frame_computer
        self.excl_joints = excl_joints
        self.max_jla_step = max_jla_step
        self._incl_mask = np.ones(7, bool)
        self._incl_mask[self._excl_js] = False
        self._p_tol = pos_tol
        self._a_tol = ang_tol
        self._max_du = max_du_step
        self._max_iter = max_iter
        self._log = logging.getLogger(self.__class__.__name__)

    def set_excl_joints(self, excl_joints: list[int]):
        """Setter method for property 'excl_joints'."""
        self._excl_js = [
            ji for ji in excl_joints
            if type(ji) == int and ji > 0 and ji < self._fc._rob_def.dof]

    def get_excl_joints(self):
        """Getter method for property 'excl_joints'."""
        return copy.copy(self._excl_js)

    excl_joints = property(get_excl_joints, set_excl_joints)

    def set_max_jla_step(self, max_jla_step: float):
        """Setter method for property 'max_jla_step'."""
        if max_jla_step < 0.0:
            self._max_jla_step = 0.0
        else:
            self._max_jla_step = max_jla_step

    def get_max_jla_step(self):
        """Getter method for property 'max_jla_step'."""
        return self._max_jla_step

    max_jla_step = property(get_max_jla_step, set_max_jla_step)

    def set_max_iter(self, max_iter: int):
        """Setter method for property 'max_iter'."""
        if max_iter < 0.0:
            self._max_iter = 0.0
        else:
            self._max_iter = max_iter

    def get_max_iter(self):
        """Getter method for property 'max_iter'."""
        return self._max_iter

    max_iter = property(get_max_iter, set_max_iter)

    def solve(
            self,
            # Starting pose in joint space
            q_start: np.ndarray,
            # Target pose in E+(3)
            p_target: m3d.Transform | m3d.PoseVector
    ) -> np.ndarray:  # Returns a solution joint pose
        """Starting from a given 'q_start' joint pose, obtain and
        return a joint pose that solves the target pose 'p_target' in
        E+(3)."""
        # self._log.debug('q_start', q_start)
        q_cur = q_start.copy()
        pv_tgt = m3d.PoseVector(p_target)
        for ij_iter in range(self._max_iter):
            self._log.info(f'Iteration {ij_iter} q_cur: {q_cur}')
            self._fc.joint_angles_vec = q_cur
            BC = self._fc.tool_pose
            self._log.debug('Current pose in base', BC)
            BT = pv_tgt.transform
            self._log.debug(f'Target pose in base: {BT}')
            CT = (BC.inverse * BT).pose_vector.array
            self._log.debug(f'Target pose in current: {CT}')
            BdCT = BC.orient @ CT
            self._log.debug('Current to target displacement in base referende: {BdCT}')
            # Calculate distance, angle, and uniform metric,
            # of the displacement
            dp = np.linalg.norm(BdCT[:3])
            da = np.linalg.norm(BdCT[3:])
            du = np.linalg.norm(BdCT)
            # Check for displacements within tolerance
            self._log.debug(f'Displacements: p = {dp}   a = {da}')
            if dp < self._p_tol and da < self._a_tol:
                break
            # Scale down to an acceptable displacement
            if du > self._max_du:
                BdCT = self._max_du * BdCT / du
                self._log.debug('Reduced current to target displacement: {BdCT}')
            # Get Jacobian on masked axes and invert
            jac = self._fc.jacobian()[:, self._incl_mask]
            try:
                u, s, vt = np.linalg.svd(jac, full_matrices=True)
                # ijac= np.linalg.pinv(j_cur, rcond=1e-5)
            except np.linalg.LinAlgError:
                self._log.warning('LinAlgError')
                q_cur = None
                break
            # Compute joints LSq-step
            pijac = vt[:len(s)].T @ np.diag(1/s) @ u.T
            dq_lsq = pijac @ BdCT
            self._log.debug('dq_lsq', dq_lsq)
            dq = dq_lsq.copy()
            # Compute null-space motion to avoid joint limits
            if self._max_jla_step > 0:
                s += 1e-5
                Q0 = vt[len(s):]
                if len(Q0) > 0:
                    self._log.debug(f'Null space basis: {Q0}')
                    qlimms = self._fc._rob_def.pos_lim_act[:, self._incl_mask]
                    q_range = qlimms[1] - qlimms[0]
                    q_centre = qlimms[0] + 0.5 * q_range
                    q_rel_off = (q_cur - q_centre) / q_range
                    # i_max = np.abs(q_rel_off).argmax()
                    # qi_rel_off = q_rel_off[i_max]
                    dq_lim = Q0.T @ Q0 @ -q_rel_off
                    n_dq_lim = np.linalg.norm(dq_lim)
                    if n_dq_lim > self._max_jla_step:
                        dq_lim *= self._max_jla_step / n_dq_lim
                    self._log.debug(f'dq_lim: {dq_lim}')
                    dq += dq_lim
            # Set the solution
            q_cur[self._incl_mask] += dq
            # Check joint limits
            qlims = self._fc._rob_def.pos_lim_act
            if not np.array_equal(q_cur, q_cur.clip(*qlims)):
                self._log.warning('Joint limit violation')
                q_cur = None
                break
        if ij_iter >= self._max_iter:
            self._log.warning(f'Beyond max iterations ({self._max_iter})')
        if q_cur is not None and du > self._max_du:
            self._log.warning('Too large task space distance')
            q_cur = None
        return q_cur
