# coding=utf-8
"""
Initialization of the 'robots' package. Special factory methods for
getting instances of definitions for robot types.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2012-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


from .robot_definition import RobotDefinition
from .nachi_sh133 import NachiSH133
from .nachi_sc15f import NachiSC15F


_robot_types = {}


def has_robot_type(robot_type):
    return robot_type in _robot_types


def get_robot_types():
    return list(_robot_types)


def register_robot_type(robot_type, robot_name=None, overwrite=False):
    """Register a robot type. 'robot_type' must be a class which inherits
    from RobotDefinition and 'robot_name' is an optional string for
    identifying the type. If an identifying string is not given, the
    class name will be used. If overwrite is True, an existing
    registration will be overwritten, otherwise an exception is
    raised.
    """
    if type(robot_type) != type or not issubclass(robot_type, RobotDefinition):
        raise Exception('A robot was registered which was not ' +
                        f'inherited from RobotDefinition: "{robot_type}"')
    if robot_name is None:
        robot_name = robot_type.__name__
    if robot_name in _robot_types:
        if overwrite:
            print('Warning: Overwriting robot type ' +
                  f'with identifier "{robot_name}"')
        else:
            raise Exception('Overwriting robot type with identifier ' +
                            f'"{robot_name}" is not allowed!')
    _robot_types[robot_name] = robot_type


def get_robot_type(robot_type):
    return _robot_types[robot_type]


# Registration of 'builtin' robot types. Only unmaintained
# Nachi-robots remain as builtins.
# for rt in (NachiSC15F, NachiSH133):
#     register_robot_type(rt)
