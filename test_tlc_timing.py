
__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2014"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@{sintef.no,ntnu.no}"
__status__ = "Development"

# // Start a controller manager by something like:
# :# chrt 99 taskset 0 python3 controller_manager.py --rob_host=mlsrm ur

tlc=cm.tlc()
t=cm.robot_facade.frame_computer()
t.pos+=m3d.Vector(0.15,0,0)
tlc.set_target_pose(t,linear_speed=0.001)
time.sleep(10.0)
tlc.abort()
sys.exit(0)
