# coding=utf-8

"""
Initialization module for the PyMoCo top level package.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2011-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import sys
import os
import pathlib

import pymoco.robots
import pymoco.simulator
import pymoco.control.facilities
import pymoco.facades
import pymoco.input


# Ensure thread switching with high time resolution. (System default
# may be as high as 5e-3)
sys.setswitchinterval(1e-5)

# Load plugins

# Packages in the plugin which will be merged into pymoco packages.
pkg_name_to_pkg = {
    'robots': pymoco.robots,
    'simulator': pymoco.simulator,
    'facades': pymoco.facades
    }

# The name of the initializer script, if necessary, which should be
# provided by the plugin in the plugin directory.
__plugin_initializer_name = '__initialize_pymoco_plugin__.py'

_pymoco_dir = pathlib.Path(__file__).parent
_plugins_dir = _pymoco_dir / 'plugins'

for pdir in _plugins_dir.iterdir():
    # Search and load recognized packages
    if pdir.is_dir():
        for plpkgp in pdir.iterdir():
            plpkgn = plpkgp.parts[-1]
            if plpkgn in pkg_name_to_pkg:
                pkg_name_to_pkg[plpkgn].__path__.extend(
                    [str(plpkgp)])
        # Execute the initializer
        if (pdir / __plugin_initializer_name).is_file():
            exec(open(pdir / __plugin_initializer_name).read())
