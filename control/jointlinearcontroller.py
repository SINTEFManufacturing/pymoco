# coding=utf-8

"""
Module for linear joint motion control.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2011-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import threading

import numpy as np

from .controller import Controller


class JointLinearController(Controller, threading.Thread):
    """ Simple class to perform linear interpolation and take target
    positions in tool flange coordinates.
    """

    def __init__(self, **kwargs):
        threading.Thread.__init__(self)
        self.daemon = True
        Controller.__init__(self, **kwargs)
        self._stop_flag = False
        self._dflt_target_speed = kwargs.get('default_target_speed', 0.1)
        self._dflt_ramp_accel = kwargs.get('default_ramp_accel', 3.0)
        self._q_target = None
        # Flags for ramping logic
        self._ramping_down = False
        self._ramping_up = False

    def abort(self, wait=True):
        """Abort the current operation."""
        self._abort = True
        if wait:
            while True:
                self._rob_fac.wait_for_control()
                if(np.linalg.norm(self._rob_fac.cmd_joint_increment)
                   / self._rob_fac.control_period) < 1.e-3:
                    break

    def get_joint_target(self):
        return self._q_target.copy()

    def set_joint_target(self, q_target, target_speed=None, ramp_accel=None):
        if not type(q_target) == np.ndarray:
            q_target = np.array(q_target)
        self._target_speed = (target_speed
                              if target_speed is not None
                              else self._dflt_target_speed)
        self._ramp_accel = (ramp_accel
                            if ramp_accel is not None
                            else self._dflt_ramp_accel)
        self._speed = min(self._target_speed,
                          self._ramp_accel * self._rob_fac.control_period)
        self._q_target = q_target
        self._path_length = np.linalg.norm(
            self._q_target - self._rob_fac.cmd_joint_pos,
            ord=np.inf)
        self._ramping_down = False
        self._ramping_up = True
        self._aborting = False
        self._abort = False
        self._idle_flag.clear()
        return True

    joint_target = property(get_joint_target, set_joint_target)

    def run(self):
        self.resume()
        self._stop_flag = False
        while not self._stop_flag:
            # Receive LLC data
            self._llc_event.wait()
            self._llc_event.clear()
            # Compose new target
            if not self.is_idle:
                period_est = self._rob_fac.control_period
                # The remaining joint travel.
                delta_q = self._q_target - self._rob_fac.cmd_joint_pos
                if self._abort and not self._aborting:
                    self._aborting = True
                    # Recompute q_target and delta_q
                    delta_q *= (self._speed**2 /
                                (2 * self._ramp_accel
                                 * np.linalg.norm(delta_q, ord=np.inf)))
                    self._q_target = self._rob_fac.cmd_joint_pos + delta_q
                # The remaining path length for the joint with the
                # longest travel.
                remaining_path = np.linalg.norm(delta_q, ord=np.inf)
                self._log.debug(f'Remaining path length {remaining_path:.4f}')
                # The current time to rest with constant acceleration,
                # adjusted with one control period
                brake_time = self._speed / self._ramp_accel + period_est
                # The travel during braking from current speed. The
                # acceleration is in the negative direction, and the
                # speed is in the positive direction.
                brake_travel = (-0.5 * self._ramp_accel * brake_time**2
                                + self._speed * brake_time)
                if remaining_path <= brake_travel:
                    if not self._ramping_down:
                        self._log.debug('Starting ramp-down')
                    self._ramping_down = True
                    if self._ramping_up:
                        self._log.debug('Ramp-down took over from ramp-up')
                        self._ramping_up = False
                    self._speed -= self._ramp_accel * period_est
                    self._log.debug(f'Ramping down to {self._speed:.3} rad/s')
                if(self._speed < self._target_speed
                   # and not self._ramping_up
                   and not self._ramping_down):
                    self._speed += self._ramp_accel * period_est
                    if self._speed > self._target_speed:
                        self._speed = self._target_speed
                        # Ramp-up ended
                        self._ramping_up = False
                        self._log.debug('Ramping up ended')
                    else:
                        self._log.debug(
                            f'Ramping up to {self._speed:.3f} rad/s')
                # The step is the speed times the period time,
                # directed along the infimum normed unit vector
                # towards the target.
                dq = self._speed * period_est * delta_q/remaining_path
                self._log.debug('Desired joint increment: {str(dq)}')
                dq_inf = np.linalg.norm(dq, ord=np.inf)
                self._log.debug(f'Desired joint increment norm: {dq_inf:.3f}')
                # time.sleep(0.001)
                if(dq_inf < remaining_path
                   # and remaining_path > 0.001
                   # and dq_inf > 0.0001
                   ):  # or self._ramping_up:
                    # Step is still smaller than remaining path,
                    # the remaining path is still significant,
                    # and the step proposed is still significant,
                    # *or* we are not ramping up.
                    self._rob_fac.cmd_joint_increment = dq
                    self._log.debug(f'Sent increment to actuators: {str(dq)}')
                else:
                    self._rob_fac.cmd_joint_increment = delta_q
                    self._log.debug(
                        'Sent remaining increment ' +
                        f'to actuators: {str(delta_q)}')
                    self._idle_flag.set()
                    self._log('Done', 2)
            else:  # Not running.
                self._rob_fac.cmd_joint_increment = self._dq_zero
            if self._llc_event.is_set():
                self._log.warn('LLC Event was set at end of cycle.')
        self._exit_event.set()
        self.suspend()

    def stop(self):
        """ Stop thread activity."""
        self._stop_flag = True
        # Make sure to wake up
        self._llc_event.set()
        # Wait for controller to complete
        self._exit_event.wait()


def _test(controller_manager, log_level=None):
    # Beware that a small joint motion is performed at low speed!
    import time
    if log_level is None:
        log_level = controller_manager._log_level
    c = controller_manager.jlc(log_level=log_level)
    j = controller_manager.robot_facade.act_joint_pos
    j[-1] += 0.05
    c.set_joint_target(j, ramp_accel=0.5, target_speed=0.1)
    c.wait_for_idle()
    # Wait for a robot delay of 10 cycles
    time.sleep(20 * controller_manager.robot_facade.control_period)
    j_err = np.linalg.norm(
        controller_manager.robot_facade.act_joint_pos - j)
    print('Joint position error at end of control: %f' % j_err)
    assert(j_err < 1e-4)
