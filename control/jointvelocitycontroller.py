# coding=utf-8

"""
Module for joint velocity control.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2011-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import threading
import time

import numpy as np

from .controller import Controller


class JointVelocityController(Controller, threading.Thread):
    """Simple class to perform linear interpolation and take target
    positions in tool flange coordinates.
    """

    def __init__(self, **kwargs):
        threading.Thread.__init__(self)
        threading.Thread.setDaemon(self, 1)
        Controller.__init__(self, **kwargs)
        self._timeout = kwargs.get('timeout', 0.1)
        self._last_command_time = time.time() - self._timeout
        self._timeout_flag = threading.Event()
        self._timeout_flag.set()
        self._stop_flag = False
        # Assume that the robot is at rest initially
        self._v_tgt = np.zeros(self._rob_def.dof, dtype=float)

    def get_timeout(self):
        return self._timeout

    def set_timeout(self, timeout):
        self._timeout = timeout

    timeout = property(get_timeout, set_timeout)

    def get_target_velocity(self):
        """Return the currently commanded joint velocity."""
        return self._v_tgt

    def set_target_velocity(self, vq_cmd):
        """ Set the commanded joint velocity."""
        if not type(vq_cmd) == np.ndarray:
            vq_cmd = np.array(vq_cmd)
        with self._llc_lock:
            self._v_tgt = vq_cmd
        self._last_command_time = time.time()
        self._timeout_flag.clear()

    target_velocity = property(get_target_velocity, set_target_velocity)

    def abort(self, wait=True):
        """Abort the twist task currently running. This is achieved by
        manipulating last command time, so that a timeout is detected.
        """
        # Make the target 0
        self.target_velocity = (0, 0, 0, 0, 0, 0)
        # Trigger timeout
        self._last_command_time = time.time() - self._timeout - 1
        if wait:
            while True:
                self._rob_fac.wait_for_control()
                if(np.linalg.norm(self._rob_fac.cmd_joint_increment)
                   / self._rob_fac.control_period) < 1.e-3:
                    break

    def run(self):
        if self._start_running:
            self.resume()
            self._log.info('Started running')
        else:
            self._log.info('Started suspended')
        self._stop_flag = False
        while not self._stop_flag:
            self._llc_event.wait()
            if self._llc_event.is_set() and not self._stop_flag:
                self._llc_event.clear()
                if(time.time() - self._last_command_time < self._timeout
                   and not self._timeout_flag.is_set()):
                    # Compose new target
                    if self. _v_tgt is not None:
                        with self._llc_lock:
                            dq = self._v_tgt * self._rob_fac.control_period
                        self._rob_fac.cmd_joint_increment = dq
                        dq_actual = self._rob_fac.cmd_joint_increment
                        if np.linalg.norm(dq_actual - dq, ord=np.inf) > 0.001:
                            self._log.warn(
                                'Desired increment could not ' +
                                f'be set. Deviation: {str(dq_actual - dq)}')
                else:
                    self._log.debug(f'Watchdog timeout at {time.time():.2f}')
                    self._timeout_flag.set()
                    # Beware: No braking, simply stop
                    self._rob_fac.cmd_joint_increment = self._dq_zero
            else:
                self._rob_fac.cmd_joint_increment = self._dq_zero
        self._exit_event.set()
        self.suspend()

    def stop(self):
        """ Stop thread activity."""
        self._stop_flag = True
        # Suppress any further motion
        self._timeout_flag.set()
        # Make sure to wake up
        self._llc_event.set()
        # Wait for controller to complete
        self._exit_event.wait()
