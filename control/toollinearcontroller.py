# coding=utf-8

"""
Module for implementing linear tool motion control.
"""

__author__ = "Morten Lind"
__copyright__ = "NTNU, SINTEF 2011-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import threading
import time
import math
import gc

import numpy as np
import math3d as m3d
import math3d.interpolation

from .kinematicscontroller import KinematicsController


class ToolLinearController(KinematicsController,
                           threading.Thread):
    """Class implementing a linear tool controller."""

    def __init__(self, **kwargs):
        """Create a tool linear controller. Supported 'kwargs' with
        default values in parentheses: 'drop_frames'(False),
        'linear_speed'(0.25), 'angular_speed'(0.75),
        'ramp_accel'(1.5), 'singular_cutoff'(1.0e-8),
        'stopping_accuracy'(1.0e-5).
        """
        KinematicsController.__init__(self, **kwargs)
        threading.Thread.__init__(self)
        self.daemon = True
        self._drop_frames = kwargs.get('drop_frames', False)
        self._sample_times = kwargs.get('sample_times', 0)
        self._dflt_lin_spd = kwargs.get('linear_speed', 0.1)
        self._dflt_ang_spd = kwargs.get('angular_speed', 0.5)
        self._dflt_ramp_acc = kwargs.get('ramp_accel', 1.5)
        if self._sample_times > 0:
            self._comp_times = -np.ones(self._sample_times, dtype=float)
        self._singular_cutoff = kwargs.get('singular_cutoff', 1.0e-8)
        # The tolerated tool point deviation from the commanded
        # trajectory point.
        self._trajectory_tolerance = kwargs.get('trajectory_tolerance', 0.0)
        # The accuracy to which the end point of a task should be
        # obtained. (Default 10um or 10urad)
        self._stopping_accuracy = kwargs.get('stopping_accuracy', 1.0e-5)
        # Estimate of the length of the arm
        self._arm_scale = 0.667 * np.sum(
            [lt.pos.length for lt in self._rob_def._link_xforms])
        self._stop_flag = False
        self._task_lock = threading.Lock()

    # def _log(self, msg, log_level=1):
    #     if log_level <= self._log_level:
    #         print(str(log_level) + ' : ' + self.__class__.__name__ + '::'
    #               + traceback.extract_stack(None, 2)[-2][2]
    #               + ' : ' + msg)

    def _ltime(self, rt=None):
        if rt is None:
            rt = time.time()
        return self._invDT*(rt-self._tstart)

    def abort(self, wait=True):
        """Abort the currently ongoing task, meaning stop motion, if any. If
        'wait' is True, do not return until rest has been achieved.
        """
        self._abort = True
        if wait:
            while True:
                self._rob_fac.wait_for_control()
                if(np.linalg.norm(self._rob_fac.cmd_joint_increment)
                   / self._rob_fac.control_period) < 1.e-3:
                    break

    def _step(self):
        """Perform one interpolation step."""
        with self._llc_lock:
            ctf = self._frame_comp(self._rob_fac.cmd_joint_pos)
            invjac = self._frame_comp.inverse_jacobian(
                rcond=self._singular_cutoff)
            llc_time = self._llc_notify_time
        period_est = self._rob_fac.control_period
        path_pos = self._metric_(self._trf0, ctf)
        if self._abort and not self._aborting:
            self._aborting = True
            self._brake_pos = min(path_pos, self._brake_pos)
            self._stop_pos = min(self._path_length,
                                 self._acc_length + path_pos)
        # Determine if we are ramping up, going flat, or ramping down.
        if (path_pos <= self._acc_length
                and path_pos < self._brake_pos):
            # Ramping up
            v = min(self._speed, math.sqrt(2 * path_pos * self._ramp_accel))
            if v == 0.0:
                # Make sure we actually get an initial speed
                v = 0.1 * self._ramp_accel * period_est
            new_path_pos = path_pos + period_est * v
            self._log.debug(
                f'Ramping UP at position {path_pos} and speed {v}')
        if path_pos >= self._brake_pos:
            # Ramping down
            vsqr = 2 * (self._stop_pos - path_pos) * self._ramp_accel
            if vsqr <= 0:
                v = 0.0
                # We're done (should be!)
                self._log.info('Task reached')
                new_path_pos = path_pos
            else:
                v = math.sqrt(vsqr)
                new_path_pos = path_pos + period_est * v
            self._log.debug(
                f'Ramping DOWN at position {path_pos} and speed {v}')
        if path_pos < self._brake_pos and path_pos > self._acc_length:
            # Going on target velocity
            new_path_pos = path_pos + period_est * self._speed
            self._log.debug(
                f'{llc_time:.4f} : Constant speed at position ' +
                f'{path_pos} and speed {self._speed}')
        if path_pos >= self._stop_pos + self._stopping_accuracy:
            self._log.warn(
                'Got path position exceeding ' +
                'path length by more than the stopping accuracy!')
        if new_path_pos < path_pos:
            self._log.warn('Got new path position ' +
                           'preceeding the current one!')
        # Get the new task space target by the interpolator
        ttf = self._tli(min(1.0, new_path_pos / self._path_length))
        # Obtain the pose vector differential (in current reference)
        Cdpose = (ctf.inverse * ttf).pose_vector.array
        # Transform to base reference
        Bdpose = ctf.orient @ Cdpose
        # Apply inverse Jacobian
        dq = invjac @ Bdpose
        # Scale the joint step linearly, so that it does not give a
        # longer than required task step
        # otf = self._frame_comp(dq + self._rob_fac.cmd_joint_pos)
        # dStep = self._metric_(ctf, otf)
        # dTarget = self._metric_(ctf, ttf)
        # if dTarget < dStep:
        #     dq *= dTarget/dStep
        # // Execute the joint step
        self._rob_fac.cmd_joint_increment = dq
        # Yield thread to allow sending joint increment in backend
        time.sleep(1e-5)
        # // Compute the obtainable tool flange for this step and
        # // abort, if the tolerance is violated
        if self._trajectory_tolerance > 0.0:
            otf = self._frame_comp(dq + self._rob_fac.cmd_joint_pos)
            if otf.pos.dist(ttf.pos) > self._trajectory_tolerance:
                self._log.error(
                    'Trajectory position' +
                    ' tolerance exceeded. Aborting!')
        # // Check if the joint step was scaled
        # dqs = self._rob_fac.cmd_joint_increment
        # if np.sum((dq - dqs) ** 2) > 0.0001:
        #     #ctfnew = self._frame_comp(dqs + self._llcQSer)
        #     ctfnew = self._frame_comp(dqs + self._rob_fac.cmd_joint_pos)
        #     new_path_pos = self._trf0._v.dist(ctfnew._v)
        #     if self._log_level >= 3:
        #         self._log('Joint increment retarded due to speed limit', 3)
        # If reached, go idle
        if path_pos >= self._stop_pos - self._stopping_accuracy:
            self._idle_flag.set()
            self._t_reached = True
        # Test if the computation went past the deadline
        if self._llc_event.is_set():
            self._log.warn('LLC event happened before this cycle.')
            if self._drop_frames:
                self._log.warn('Dropping control frame.')
                self._llc_event.clear()

    def _pos_metric(self, t0, t1):
        return t0._v.dist(t1._v)

    def _ang_metric(self, t0, t1):
        return t0._o.ang_dist(t1._o)

    def set_target_pose(self,
                        target_pose: m3d.Transform,
                        linear_speed: float = None,
                        angular_speed: float = None,
                        ramp_accel: float = None,
                        do_scale: bool = True,
                        express_frame: str = 'Base'):
        """Perform a linear motion from current tool pose to the given
        'target_pose', limited by 'linear_speed' [m/s] or
        'angular_speed' [rad/s] and a 'ramp_accel' [m/s^2] or
        [rad/s^2] acceleration for ramping the speed up and down. The
        'express_frame' determines in what reference the 'target_pose'
        is given; either 'Tool' or 'Base'. Here 'Tool' means the
        instantaneous tool reference frame at the start of the motion;
        i.e. not the moving tool frame during the motion. Caution: Be
        very careful and aware with the 'express_frame' parameter, and
        be certain that it corresponds to the 'target_pose'!
        """
        # Setup, check, and start task
        with self._task_lock:
            if not self.is_idle:
                # Reject setting new target while running.
                task_accepted = False
            else:
                self._log.info(f'Accepting task at time {time.time()} s')
                self._do_scale = do_scale
                self._t_reached = False
                self._abort = False
                self._aborting = False
                self._linear_speed = (
                    linear_speed if linear_speed is not None
                    else self._dflt_lin_spd)
                self._angular_speed = (
                    angular_speed if angular_speed is not None
                    else self._dflt_ang_spd)
                self._ramp_accel = (
                    ramp_accel if ramp_accel is not None
                    else self._dflt_ramp_acc)
                self._trf0 = self._frame_comp(self._rob_fac.cmd_joint_pos)
                if express_frame == 'Tool':
                    # Get base representation of the tool target.
                    target_pose = self._trf0 * target_pose
                self._trf1 = m3d.Transform(target_pose)
                self._log.debug(f'Current pose : {str(self._trf0)}')
                self._log.debug(f'Target pose : {str(self._trf1)}')
                ang_dist = self._trf0.orient.ang_dist(self._trf1.orient)
                lin_dist = self._trf0.pos.dist(self._trf1.pos)
                # Determine if motion is angular or linear dominated
                # by time required for motion
                if(0.5 * self._arm_scale * ang_dist / self._angular_speed
                   > lin_dist / self._linear_speed):
                    self._metric_ = self._ang_metric
                    self._speed = self._angular_speed
                    self._log.debug('Angular bounded motion.')
                else:
                    self._metric_ = self._pos_metric
                    self._speed = self._linear_speed
                    self._log.debug('Linearly bounded motion.')
                self._path_length = self._metric_(self._trf0, self._trf1)
                self._stop_pos = self._path_length
                if self._path_length < self._stopping_accuracy:
                    # Do not accept a task with end point within the
                    # stopping accuracy
                    task_accepted = False
                else:
                    self._tli = m3d.interpolation.SE3Interpolator(
                        self._trf0, self._trf1)
                    self._acc_length = (
                        0.5 * self._speed ** 2 / self._ramp_accel)
                    self._brake_pos = (
                        self._path_length - self._acc_length
                        - self._speed * self._rob_fac.control_period)
                    self._log.debug(f'Starting : BP={self._brake_pos:.3f} ' +
                                    f'PL={self._path_length:.3f} ' +
                                    f'AL={self._acc_length:.3f}')
                    if 2 * self._acc_length > self._path_length:
                        self._brake_pos = 0.5 * self._path_length
                    self._tstart = time.time()
                    self._idle_flag.clear()
                    task_accepted = True
        return task_accepted

    def get_target_pose(self):
        return self._trf1

    target_pose = property(get_target_pose, set_target_pose)

    def run(self):
        if self._start_running:
            self.resume()
            self._log.info('Started running')
        else:
            self._log.info('Started suspended')
        self._stop_flag = False
        self._idle_flag.set()
        # Disable garbage collection and control when to collect
        gc.disable()
        while not self._stop_flag:
            self._llc_event.wait()
            if self._stop_flag:
                break
            self._llc_event.clear()
            t_start = time.time()
            if not self.is_idle:
                self._step()
                if self._sample_times > 0:
                    t_end = time.time()
                    self._comp_times = np.roll(self._comp_times, 1)
                    self._comp_times[0] = t_end - t_start
            else:
                self._rob_fac.cmd_joint_increment = self._dq_zero
            # This is a good time to collect garbage
            gc.collect()
        # Generally enable garbage collection again
        gc.enable()
        self._exit_event.set()
        self.suspend()

    def stop(self):
        self._stop_flag = True
        # Make sure to wake up
        self._llc_event.set()
        # Wait for control completion
        self._exit_event.wait()
