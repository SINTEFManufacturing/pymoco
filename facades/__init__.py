# coding=utf-8
"""
Package for definition and provision of standard robot system facades.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2011-2021"
__credits__ = ["Morten Lind", "Johannes Schrimpf"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


from .robot_facade import RobotFacade
# from .nachi_olimex_legacy import NachiOlimexLegacyFacade

_facade_types = {}


def get_facade_types():
    return list(_facade_types.keys())


def get_facade_type(facade_name):
    return _facade_types[facade_name]


def register_facade_type(facade_type, facade_name=None, overwrite=False):
    """Register a facade type. 'facade_type' must be a class which
    inherits from RobotFacade and 'facade_name' is an optional string
    for identifying the type. If an identifying string is not given,
    the class name will be used. If overwrite is True, an existing
    registration will be overwritten, otherwise an exception is
    raised.
    """
    if type(facade_type) != type or not issubclass(facade_type, RobotFacade):
        raise Exception('A facade was registered which ' +
                        f'was not inherited from RobotFacade: "{facade_type}"')
    if facade_name is None:
        facade_name = facade_type.__name__
    if facade_name in _facade_types:
        if overwrite:
            print(
                f'Warning: Overwriting facade with identifier "{facade_name}"')
        else:
            raise Exception('Overwriting facade with identifier ' +
                            f'"{facade_name}" is not allowed!')
    _facade_types[facade_name] = facade_type


# Unmaintained facades:
# [NachiOlimexLegacyFacade]
