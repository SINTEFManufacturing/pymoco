# coding=utf-8
"""
Module providing a generic time event publisher.
"""

__author__ = "Johannes Schrimpf"
__copyright__ = "SINTEF Manufacturing 2011-2014"
__credits__ = ["Johannes Schrimpf", "Morten Lind", "Lars Tingelstad"]
__license__ = "LGPLv3"
__maintainer__ = "Johannes Schrimpf"
__email__ = "johannes.schrimpf(_at_)itk.ntnu.no"
__status__ = "Development"

import threading
import time

class EventPublisher:
    def __init__(self):
        self._subscribers = []
        self._event_cond = threading.Condition()

    def subscribe(self, subscriber):
        """
        Register a new function that is called when an event occurs

        Arguments:
        subscriber -- The function to call when a new event occurs
        """
        if subscriber not in self._subscribers:
            self._subscribers.append(subscriber)

        
    def unsubscribe(self, subscriber):
        """
        Remove a subscriber

        Arguments:
        subscriber -- The subscriber to remove
        """
        if subscriber in self._subscribers:
            self._subscribers.remove(subscriber)

    def publish(self, event_time=None):
        """
        Notify the subscribers
        """
        with self._event_cond:
            self._event_cond.notify_all()
        if event_time is None:
            event_time = time.time()
        for subscriber in self._subscribers:
            subscriber(event_time)

    def wait_for_event(self, timeout=None):
        """Block until next event."""
        with self._event_cond:
            return self._event_cond.wait(timeout)
