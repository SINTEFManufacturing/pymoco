# coding=utf-8

"""
Module for defining the base class and interface for all robot system facades.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2011-2021"
__credits__ = ["Morten Lind", "Johannes Schrimpf"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

import traceback
import threading
import sys
import logging

from .event_publisher import EventPublisher


# Ensure thread switching with high time resolution. (System default
# may be as high as 5e-3)
sys.setswitchinterval(1e-5)


class RobotFacade(object):

    joint_spaces = ['actuator', 'serial', 'encoder']

    class JointLimitViolationError(Exception):
        def __init__(
                self,
                jpos_violation: tuple[tuple[float], tuple[float]]):
            Exception.__init__(self, str(jpos_violation))

    def __init__(self, **kwargs):
        joint_space = kwargs.get('joint_space', 'serial')
        self._tracking_tol = kwargs.get('tracking_tol', 0.1)
        self._cycle_time = self._cycle_time_nom = kwargs.get('cycle_time', 0.1)
        if joint_space in RobotFacade.joint_spaces:
            self._joint_space = joint_space
        else:
            raise Exception('Tried to set unknown joint_space: "{}"'
                            .format(joint_space))
        self._log_level = kwargs.get('log_level', logging.INFO)
        self._log = logging.getLogger(self.__class__.__name__)
        self._log.setLevel(self._log_level)
        self._event_publisher = EventPublisher()
        self._control_cond = threading.Condition()
        # Cached variables to be book kept by the concrete facade
        self._q_tracked = None
        self._q_actual = None
        self._q_increment = None
        self._q_error = None
        self._t_packet = None

    def wait_for_control(self, timeout=None):
        """Wait for an active controller to completed a control cycle
        towards the robot system. This happens when a new joint
        position target or increment has been set.
        """
        with self._control_cond:
            self._control_cond.wait(timeout)

    def subscribe(self, subscriber):
        """Subscribe to real-time events from the robot
        system. 'subscriber' must be a callable taking one
        parameter. When called, the argument to 'subscriber' will be
        the earliest known receive time of the event.
        """
        self._event_publisher.subscribe(subscriber)

    def unsubscribe(self, subscriber):
        """Remove the 'subscriber' from listening for events."""
        self._event_publisher.unsubscribe(subscriber)

    def wait_for_event(self, timeout=None):
        """Wait for the next real-time event from the robot
        system.
        """
        self._event_publisher.wait_for_event(timeout)

    def get_robot_definition(self):
        """Return the robot definition in use by the facade. This is the
        access to the kinematics and configuration.
        """
        return self._rob_def

    robot_definition = property(get_robot_definition)

    def get_frame_computer(self):
        """Return the frame computer in use by the facade. This is the access
        to the kinematics and current configuration.
        """
        return self._frame_computer

    frame_computer = property(get_frame_computer)

    def get_joint_space(self):
        return self._joint_space

    joint_space = property(get_joint_space)

    def get_control_period(self):
        """Return the current estimate of the cycle time in the interaction
        with the robot controller.
        """
        return self._cycle_time

    control_period = property(get_control_period)

    def get_current_arrival_time(self):
        """Return the time at which the current, i.e. latest, status or
        synchronization packet was received from the robot
        controller.
        """
        return self._t_packet

    current_arrival_time = property(get_current_arrival_time)

    def get_q_zero_offsets(self):
        """Return the actual zero offset used for the specific robot
        controlled."""
        raise NotImplementedError(
            '"' + traceback.extract_stack(limit=1)[-1][2] + '"'
            + ' not implemented in abstract base class "RobotFacade"')

    q_zero_offsets = property(get_q_zero_offsets)

    def get_cmd_joint_pos(self):
        """Get the latest commanded joint positions."""
        return self._q_tracked.copy()

    def set_cmd_joint_pos(self, joint_pos):
        """Set the commanded joint positions. This methods must updated
        internal variables _q_tracked and _q_increment.
        """
        raise NotImplementedError(
            '"' + traceback.extract_stack(limit=1)[-1][2] + '"'
            + ' not implemented in abstract base class "RobotFacade"')

    cmd_joint_pos = property(get_cmd_joint_pos, set_cmd_joint_pos)

    def check_joint_pos_limits(
            self, jpos: tuple[float]
    ) -> tuple[tuple[float], tuple[float]]:
        return (
            jpos < self._rob_def._pos_lim_act[0],
            jpos > self._rob_def._pos_lim_act[1])

    def get_act_joint_vel(self):
        """Get the actual joint velocity as reported from the robot system, or
        inferred from the history actual positions.
        """
        raise NotImplementedError(
            '"' + traceback.extract_stack(limit=1)[-1][2] + '"'
            + ' not implemented in abstract base class "RobotFacade"')

    act_joint_vel = property(get_act_joint_vel)

    def get_act_joint_pos(self):
        """Get the actual joint positions as reported from the robot
        system.
        """
        return self._q_actual.copy()

    act_joint_pos = property(get_act_joint_pos)

    def get_cmd_joint_increment(self):
        """Get the latest commanded increment in joint positions."""
        return self._q_increment.copy()

    def set_cmd_joint_increment(self, joint_inc):
        """Command an increment in joint positions."""
        self.set_cmd_joint_pos(joint_inc + self._q_tracked)

    cmd_joint_increment = property(get_cmd_joint_increment,
                                   set_cmd_joint_increment)

    def stop(self, join=False):
        """Stop the interaction with the robot system."""
        raise NotImplementedError(
            '"' + traceback.extract_stack(limit=1)[-1][2] + '"'
            + ' not implemented in abstract base class "RobotFacade"')
