#!/usr/bin/env python3
# coding=utf-8
"""
Controller manager module. A controller handles the interfacing to a
robot controller, and manages motion controllers working with that
robot.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2012-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import time
import threading
import logging
import pathlib

import numpy as np
import math3d as m3d

import pymoco.control as pctl
import pymoco.facades


logging.basicConfig()


class ControllerManager(object):
    """ Basic management of motion generators to ensure real-time
    constraints and policies for switching controllers."""
    def __init__(self,
                 rob_type: str = None,
                 facade_name: str = None,
                 rob_host: str = '127.0.0.1',
                 rob_out_port: int = None,
                 rob_in_port: int = None,
                 rob_port: int = None,
                 controller: str = '',
                 log_level: int = logging.INFO,
                 **kwargs):
        global pymoco
        self._cm_lock = threading.Lock()
        self._log_level = log_level
        self._log = logging.getLogger(self.__class__.__name__)
        self._log.setLevel(self._log_level)
        kwargs['facade_name'] = facade_name
        kwargs['rob_type'] = rob_type
        kwargs['rob_host'] = rob_host
        if rob_port is not None:
            kwargs['rob_port'] = rob_port
        if rob_out_port is not None:
            kwargs['rob_out_port'] = rob_out_port
        if rob_in_port is not None:
            kwargs['rob_in_port'] = rob_in_port
        kwargs['log_level'] = log_level
        # kwargs[''] =
        self._controller = None
        # RobotFacade factory
        self._rob_fac = pymoco.facades.get_facade_type(
            kwargs['facade_name'])(**kwargs)
        # Some facades need an active controller before connecting (starting)!
        if controller != '':
            self._log.info(f'Starting {controller} controller')
            exec('self.%s()' % controller)
        else:
            self._log.info('Starting default Zero Velocity Controller.')
            self.zvc()
        self._log.info('Starting robot facade')
        self._rob_fac.start()
        self._stopped = False
        # Give some time for the robot facade to start
        time.sleep(0.1)

    def get_robot_facade(self):
        """ Give direct access to the underlying robot system
        interface. This gives an umnanaged access to the underlying
        kinematics systems that may severey affect real-time operation
        of certain controllers. Use with care."""
        return self._rob_fac
    robot_facade = property(get_robot_facade)

    def get_controller_type_name(self):
        """ Return the name of the type (or class) of the running
        controller."""
        return self._controller.__class__.__name__

    controller_type_name = property(get_controller_type_name)

    def get_current_controller(self):
        """ Give access to the active controller."""
        return self._controller

    current_controller = property(get_current_controller)

    def get_tool_xform(self):
        """ Return the tool transform used in the kimematics
        computations. The tool transform is the homogeneous, static
        transform from operation (tool) coordinates to the robot tool
        flange."""
        return self._rob_fac.frame_computer.tool_xform

    def set_tool_xform(self, new_tool_xform):
        """ Set the specific tool transform used in the kimematics
        computations. This is done by a managed switch to the zero
        velocity controller."""
        # if type(self._controller) == pctl.ZeroVelocityController:
        #     self._rob_fac.frame_computer.tool_xform = new_tool_xform
        # else:
        #     log('Warning: Tried to set tool transform while controller operating.', 1)
        with self._cm_lock:
            self._controller.wait_for_idle()
            self._rob_fac.frame_computer.tool_xform = new_tool_xform
            # if type(self._controller) != pctl.ZeroVelocityController:
            #     new_controller = pctl.ZeroVelocityController(robot_facade=self._rob_fac, start_running=False)
            #     new_controller.start()
            #     if not self._controller is None:
            #         self._controller.wait_for_idle()
            #         self._controller.stop()
            #     new_controller.resume()
            #     self._controller = new_controller
            #     self._rob_fac.frame_computer.tool_xform = new_tool_xform

    tool_xform = property(get_tool_xform, set_tool_xform)

    def start_controller(self, controller_class, **kwargs):
        with self._cm_lock:
            if type(self._controller) != controller_class or len(kwargs) > 0:
                if 'log_level' not in kwargs:
                    kwargs['log_level'] = self._log_level
                new_controller = controller_class(robot_facade=self._rob_fac,
                                                  start_running=False,
                                                  **kwargs)
                new_controller.start()
                if self._controller is not None:
                    # Wait for task-oriented controllers to finish
                    self._controller.wait_for_idle()
                    # Command continuous controllers to stop motion
                    self._controller.abort()
                    # Wait for end of a control cycle
                    self._rob_fac.wait_for_control()
                    # Suspend the current, and resume the new controller
                    self._controller.suspend()
                    new_controller.resume()
                    # Take down the old controller.
                    self._controller.stop()
                else:
                    # There was no running controller, simply start
                    # the new one
                    new_controller.resume()
                self._controller = new_controller
            return self._controller

    def jlc(self, **kwargs):
        return self.start_controller(pctl.JointLinearController, **kwargs)

    def jvc(self, **kwargs):
        return self.start_controller(pctl.JointVelocityController, **kwargs)

    def tlc(self, **kwargs):
        return self.start_controller(pctl.ToolLinearController, **kwargs)

    def tcc(self, **kwargs):
        return self.start_controller(pctl.ToolCorrectionController, **kwargs)

    def pcc(self, **kwargs):
        return self.start_controller(pctl.PathCorrectionController, **kwargs)

    def tvc(self, **kwargs):
        return self.start_controller(pctl.ToolVelocityController, **kwargs)

    def zvc(self, **kwargs):
        return self.start_controller(pctl.ZeroVelocityController, **kwargs)

    def stop(self, join=False):
        if not self._stopped:
            self._stopped = True
            self._controller.stop()
            self._rob_fac.stop(join)
            if join:
                self._controller.join()

# Backward compatibility:


def cm_parser():
    """ Create a default controller manager parser."""
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('facade_name',
                        type=str, choices=pymoco.facades.get_facade_types())
    parser.add_argument('rob_type',
                        type=str, choices=pymoco.robots.get_robot_types())
    parser.add_argument('--controller',
                        type=str,
                        choices=[
                            'tlc', 'tvc', 'jlc', 'jvc', 'zvc'], default='zvc')
    parser.add_argument('--rob_host', type=str, default='127.0.0.1')
    parser.add_argument('--rob_out_port',
                        type=int, default=pymoco.input.llc_out_port)
    parser.add_argument('--rob_in_port',
                        type=int, default=pymoco.control.llc_in_port)
    parser.add_argument('--rob_port', type=int, default=None)
    parser.add_argument('--bind_host', type=str, default='0.0.0.0')
    parser.add_argument('--encoder_offsets',
                        type=pathlib.Path, default=pathlib.Path(''),
                        help="""File which holds the encoder offsets for the specific robot in
                        .npy- or .npy.txt-format."""
                        )
    parser.add_argument('--log_level', type=int, default=logging.INFO)
    return parser


if __name__ == '__main__':
    parser = cm_parser()
    args = parser.parse_args()
    LOG_LEVEL = args.log_level
    if args.rob_port is None:
        del args.rob_port
    if args.rob_type != '':
        if args.encoder_offsets.is_file():
            if args.encoder_offsets.suffix == '.txt':
                args.encoder_offsets = np.loadtxt(str(args.encoder_offsets))
            elif args.encoder_offsets.suffix == '.npy':
                args.encoder_offsets = np.load(str(args.encoder_offsets))
        else:
            del args.encoder_offsets
        con_man = ControllerManager(**vars(args))
        # Shortcuts
        cm = con_man
        rf = cm.robot_facade
        fc = rf.frame_computer
        # setup_test_transforms()
        # starttvc = con_man.tvc
        # starttlc = con_man.tlc
        import atexit
        atexit.register(con_man.stop)
    # import code
    # code.interact('', None, globals())
